$("#contact-form").submit(function(e) {

			e.preventDefault();
			console.log("Contacto---");

			var c_name = $("#c_name").val();
			var c_email = $("#c_email").val();
			var c_message = $("#c_message ").val();
			var c_subject = $("#c_subject ").val();
			var c_terms = $("#c_terms ").val();
			console.log(c_terms);
			var responseMessage = $('#contact-form .ajax-response');

				if (( c_subject== "" ||c_name== "" || c_email == "" || c_message == "") || (!isValidEmailAddress(c_email) )) {
					responseMessage.fadeIn(500);
					responseMessage.html('Llena todos los campos.');
				}else {
					$.ajax({
						type: "POST",
						url: "assets/php/contactForm.php",
						dataType: 'json',
						data: {
							c_email: c_email,
							c_name: c_name,
							c_message: c_message,
							c_subject: c_subject,
							
						},
						beforeSend: function(result) {
							$('#contact-form button').empty();
							$('#contact-form button').append('Enviando...');
						},
						success: function(result) {
							if(result.sendstatus == 1) {
								$('#contact-form .ajax-hidden').fadeOut(500);
								responseMessage.html(result.message).fadeIn(500);
							} else {
								$('#contact-form button').empty();
								$('#contact-form button').append('Try again.');
								responseMessage.html(result.message).fadeIn(1000);
							}
						}
					});
				}
			
			return false;

		});


function isValidEmailAddress(emailAddress) {
			var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
			return pattern.test(emailAddress);
		};